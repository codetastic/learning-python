# Learning python
I guess I'm using this README as a blog for the time being. This is what I've been up to so far:
Not sure where I am going with this yet but I've started going through the book Learn Python 3 The Hard Way.
Still fairly early on in the book, with the goal of finishing at least five exercises per day.

Doing a non book related project given to me by another twitch user with a base script (a little script to keep a record of scores in games) and a
set of TODO to practice with. Currently stumped on how to get it to work properly with a while loop.

scorekeeper.py is the original non looping version that I added the functionality to include the name of the game

scorekeeperloop.py is the script I am currently working on

score.csv is sample output