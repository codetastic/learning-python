# TODO: Add a comment above each line explaining what it does
# TODO: Add the ability to specify a game
# TODO: Make it so you don't have to run the script each time (while loop!) <- semi stuck here, I got it working with just upper case N but how to add lower case? "N" or "n" don't 
# seem to work am I just using the "or" keyword incorrectly? Update: Turns out there is something called upper() I can use to force an upper case N so now it works as intended.
# TODO: Convert this to use the Python built-in CSV library

# imports datetime from the python library
from datetime import datetime
# create the variable to check to end the loop empty for now to be filled via input by user
end_loop = ()

# loop the following code until the condition is met
while end_loop != "N":
    # record the name of the game via input from user
    game_name = input("What game are you recording a score for? ")
    # record the score
    game_score = input("What score did you get? ")
    # Still a bit iffy on the "with" keyword but just know it is used here to manipulate a file, "score.csv is the file and "a" means append to the file "as" assigns an alias (basically a nickname) to be used in reference to the file
    with open("score.csv", "a") as scoreFile:
        # Using the alias in conjunction with .write to append to the score.csv file, entering the date and time, the name of the game and the score
        scoreFile.write(f"{datetime.now()}, {game_name}, {game_score}\n")
        # Determine if the user wants to add another score or not, if they respond with anything besides the letter N it will continue
    end_loop = input("Do you want to add a score? Y/N ")
    # If the user inputs a lower case n this forces it to be an upper case N and ends the loop
    end_loop = end_loop.upper()
