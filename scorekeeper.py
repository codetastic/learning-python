# TODO: Add a comment above each line explaining what it does
# TODO: Add the ability to specify a game
# TODO: Make it so you don't have to run the script each time (while loop!)
# TODO: Convert this to use the Python built-in CSV library

# importing the datetime function from the python library
from datetime import datetime

# take input from the user to set the value of the score variable
game = input("What game are you recording a score for? ")
score = input("What score did I get? ")

# not sure what is happening here, "a"ppending to a file called score.csv but don't know what "with" does, and assuming "as f" means as a file? Found out it means alias.
# changed f to scoreFile just to illustrate that it isn't a special keyword but it is setting an alias, not sure why we need to use an alias here? I guess because we couldn't do
# with open("score.csv", "a"):
#      score.csv.write? Because the syntax would be wrong? Theres no variable set that contains the file and extention? Can you have references to files and extensions in a variable?
with open("score.csv", "a") as scoreFile:
    scoreFile.write(f"{datetime.now()}, {game}, {score}\n") 